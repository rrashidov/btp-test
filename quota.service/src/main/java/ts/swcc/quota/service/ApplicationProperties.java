package ts.swcc.quota.service;

public class ApplicationProperties {
    private String tokenUrl = null, clientId = null, username = null, privateKey = null, userId = null,
            companyId = null;
    private boolean useUserNameAsUserId = false;
    private int expireInMinutes = 10;

    public ApplicationProperties(String tokenUrl, String clientId, String username, String privateKey, String userId,
            Integer expireInMinutes, String companyId) {
        super();
        this.tokenUrl = tokenUrl;
        this.clientId = clientId;
        this.username = username;
        this.privateKey = privateKey;
        this.userId = userId;
        this.useUserNameAsUserId = false;
        this.expireInMinutes = expireInMinutes;
        this.companyId = companyId;

        expireInMinutes = expireInMinutes != null && expireInMinutes > 0 ? expireInMinutes : 10;

        if ((userId == null || userId.trim().length() == 0) && (username != null && username.length() != 0)) {
            System.out.println("Using username as userId...");
            userId = username;
            useUserNameAsUserId = true;
        }
    }

    public boolean isPropertyMissing() {
        if (tokenUrl != null && clientId != null && privateKey != null && userId != null) {
            return false;
        }

        return true;
    }

    public String getTokenUrl() {
        return tokenUrl;
    }

    public void setTokenUrl(String tokenUrl) {
        this.tokenUrl = tokenUrl;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean useUserNameAsUserId() {
        return useUserNameAsUserId;
    }

    public void setUseUserNameAsUserId(boolean useUserNameAsUserId) {
        this.useUserNameAsUserId = useUserNameAsUserId;
    }

    public int getExpireInMinutes() {
        return expireInMinutes;
    }

    public void setExpireInMinutes(int expireInMinutes) {
        this.expireInMinutes = expireInMinutes;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

}
