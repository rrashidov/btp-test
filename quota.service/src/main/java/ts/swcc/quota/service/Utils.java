package ts.swcc.quota.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletContext;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

public class Utils {
    public static ApplicationProperties getProperties() {
        Properties properties = new Properties();
        BufferedReader bufferedReader;
        try {
            PathMatchingResourcePatternResolver matcher = new PathMatchingResourcePatternResolver();
            Resource resource = matcher.getResource("service.properties");
            bufferedReader = new BufferedReader(new FileReader(resource.getURI().getPath()));
            properties.load(bufferedReader);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ApplicationProperties props = new ApplicationProperties(
                properties.getProperty("tokenUrl"),
                properties.getProperty("clientId"),
                properties.getProperty("userName"),
                properties.getProperty("privateKey"),
                properties.getProperty("userId"),
                Integer.decode(properties.getProperty("expireInMinutes")),
                properties.getProperty("companyId"));

        return props;
    }
}
